#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 21 17:12:34 2017

@author: Guest
"""

# In[] 
folder_path = "/Users/Guest/Documents/Yuxin/experiment/output/"
file = "textsum_m1_epoch13_pred"
file_path = folder_path + file + ".txt"
output_file = folder_path + file + "_detagged.txt"
# In[] 
outF = open(output_file, "w")
with open(file_path) as fp:  
    line = fp.readline()
    count = 1
    while line:
        #print(count)
        #print("{}".format(line.strip()))
        line = line.strip()
        #print(line)
        try:
            tok_list = line.split()
            for i, item in enumerate(tok_list):
                if item.isupper():
                    del tok_list[i]
            print(" ".join(tok_list))
            #print(line)
            outF.write(" ".join(tok_list))
        except:
            outF.write(line)
            #outF.write("\n")
        outF.write("\n")                       
        line = fp.readline()
        count += 1
outF.close()