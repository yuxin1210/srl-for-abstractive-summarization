#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 19:13:52 2017

@author: yuxin
"""
# In[]
from __future__ import unicode_literals, print_function
import spacy
from spacy.pipeline import DependencyParser
nlp = spacy.load('en')
import operator
import nltk

# In[]
def leaves(tree):
    """Finds NP (nounphrase) leaf nodes of a chunk tree."""
    for subtree in tree.subtrees(filter = lambda t: t.label()=='NP'):
        yield subtree.leaves()
       
def get_terms(tree):
    terms = []
    for leaf in leaves(tree):
        term = [ w for w,t in leaf]
        terms.append(term)
    for leaf in tree.leaves():
        if 'PRP' in leaf[1]:
            terms.append([leaf[0]])
    return terms
# In[]
def make_term_idx(doc,terms):
    term_idx = dict()
    #scan_pt = len(doc)-1
    for term in terms:
        np = " ".join(term)
        for token in list(doc):
            if str(token) in term:
                if len(term) == 1 or str(doc[token.i+1]) in term:
                    begin_of_term = token.i
                    #print(token,end_of_term)
                    end_of_term = token.i + len(term) - 1
                    #print(end_of_term,begin_of_term)
                    term_idx[np] = (begin_of_term,end_of_term)
                    #scan_pt = begin_of_term -1
                    break
    return term_idx

def augment(doc):
    toks = [(str(token)) for token in doc]
    # make list of postoks for NP extracting
    postoks = [(str(token),token.tag_) for token in doc]
    tree = chunker.parse(postoks)
    terms = get_terms(tree)
    term_idx = make_term_idx(doc,terms)
    sorted_term_idx = sorted(term_idx.items(), key=operator.itemgetter(1))
    for term in sorted_term_idx[::-1]:
        np = term[0]
        (begin_of_term,end_of_term) = term_idx[np]
        for token in doc[begin_of_term:end_of_term+1]:
            if 'subj' in token.dep_:           
                toks.insert(end_of_term+1,'ESUBJ')
                toks.insert(begin_of_term,'BSUBJ')
                break
            elif 'obj' in token.dep_:
                toks.insert(end_of_term+1,'EOBJ')
                toks.insert(begin_of_term,'BOBJ')
                break  
    return toks
# In[] 
#Taken from Su Nam Kim Paper...
grammar = r"""
    NBAR:
        {<NN.*|JJ>*<NN.*>}  # Nouns and Adjectives, terminated with Nouns
        
    NP:      
        {<DT>?<CD>?<NBAR><IN|CC><DT>?<CD>?<NBAR>} # connected with in/of/etc...
        {<DT>?<CD>?<NBAR>}
        
"""
chunker = nltk.RegexpParser(grammar)
# In[]
import re
p = re.compile('#*[.,]?#+')

#filepath = '/Users/Guest/Documents/Yuxin/experiment/random.title.50.txt'
#filepath = '/Users/yuxin/Yuxin/TokyoTechWinterProgram/experiment/data/random.title.20.txt'
filepath = '/Users/yuxin/sumdata/train/valid.title.filter.txt'
output_file = '/Users/yuxin/Yuxin/TokyoTechWinterProgram/experiment/data/valid.title.filter.augmented.txt'
outF = open(output_file, "a")

with open(filepath) as fp:  
    line = fp.readline()
    count = 1
    while line:
        if(count>=185175):
            print(count)
            #print("{}".format(line.strip()))
            line = line.strip()
            #print(line)
            try:
                line = p.subn('#', line)[0]
                #print(line)
                doc = nlp(line.decode('utf-8'))
                #print(doc)
                augmented = augment(doc)
                #print(" ".join(augmented))
                outF.write(" ".join(augmented))
            except:
                outF.write(line)
            outF.write("\n")                       
        line = fp.readline()
        count += 1
outF.close()
# In[]
# from beginning

import re
p = re.compile('#*[.,]?#+')

#filepath = '/Users/Guest/Documents/Yuxin/experiment/random.title.50.txt'
#filepath = '/Users/yuxin/Yuxin/TokyoTechWinterProgram/experiment/data/random.title.20.txt'
filepath = '/Users/yuxin/sumdata/train/train.title.txt'
output_file = '/Users/yuxin/Yuxin/TokyoTechWinterProgram/experiment/data/train.title.augmented.txt'
outF = open(output_file, "a")

with open(filepath) as fp:  
    line = fp.readline()
    count = 1
    while line:
        print(count)
        #print("{}".format(line.strip()))
        line = line.strip()
        #print(line)
        try:
            line = p.subn('#', line)[0]
            #print(line)
            doc = nlp(line.decode('utf-8'))
            #print(doc)
            augmented = augment(doc)
            #print(" ".join(augmented))
            outF.write(" ".join(augmented))
        except:
            outF.write(line)
        outF.write("\n")                       
        line = fp.readline()
        count += 1
outF.close()