#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 14 22:43:53 2018

@author: yuxin
"""

input_filepath = "/Users/yuxin/sumdata/DUC2004/task1_ref0.txt"
length_list = []
count = 0
with open(input_filepath) as fp: 
    line = fp.readline()
    while line:
        count += 1
        #print(count)
        #print("{}".format(line.strip()))
        line = line.strip()
        toks = line.split()
        length_list.append(len(toks))
        line = fp.readline()
print("count: ", count)

import numpy as np
mean = np.mean(length_list)
sample_var = np.var(length_list,ddof=1)
ci_lb = mean - 1.96*sample_var/np.sqrt(count)
ci_ub = mean + 1.96*sample_var/np.sqrt(count)
print("mean: ", mean)
print("sample var: ",sample_var)
print("95% confidence interval: [{},{}]".format(ci_lb,ci_ub))