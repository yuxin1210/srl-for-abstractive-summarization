#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 02:28:15 2018

@author: yuxin
"""


import re
p = re.compile('< unk >')

filepath = "/home/lr/yuxin/sumdata/train/valid.title.filter.augsrl.txt"
#output_file = '/Users/yuxin/sumdata/train/train.title.augsrl.txt'
output_file = '/home/lr/yuxin/sumdata/train/valid.title.filter.augsrl.v2.txt'
#filepath = "/Users/Guest/Documents/Yuxin/experiment/random.title.50.txt"
#output_file =  "/Users/Guest/Documents/Yuxin/experiment/random.title.50.augsrl.txt"
outF = open(output_file, "w")

with open(filepath) as fp:  
    line = fp.readline()
    count = 1
    while line:
        print(count)
        #print("{}".format(line.strip()))
        line = line.strip()
        #print(line)
        try:
            line = p.subn('<unk>', line)[0]
            outF.write(line)
        except:
            outF.write(line)
        outF.write("\n")                       
        line = fp.readline()
        count += 1
outF.close()