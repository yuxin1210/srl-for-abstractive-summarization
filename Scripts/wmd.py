#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  7 18:03:57 2018

@author: yuxin
"""

# In[]
from shorttext.metrics.wasserstein import word_mover_distance
from shorttext.utils import load_word2vec_model
import string
# In[]