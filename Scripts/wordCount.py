#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 22:38:12 2018

@author: yuxin
"""
import matplotlib.pyplot as plt
import numpy as np

input_filepath = "/Users/yuxin/sumdata/DUC2004/task1_ref3.txt"

save_path = "/Users/yuxin/Yuxin/TokyoTechWinterProgram/Research Record/report_figures/"

word_count = []
with open(input_filepath) as fp: 
    line = fp.readline()
    while line:
        line = line.strip()
        word_count.append(len(line.split()))
        line = fp.readline()

n, bins, patches = plt.hist(word_count,
                            bins=np.arange(min(word_count)-0.5, max(word_count)+0.5),
                            ec='k',alpha=0.6)
plt.xlabel("Length")
plt.ylabel("Frequency")  
fig_name = input_filepath.split("/")[-1].split(".")[0]+"_len_hist.png" 
plt.savefig(fname=save_path+fig_name)   
