#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 15:35:10 2017

@author: Guest
"""
# In[] 
import nltk
from pntl.tools import Annotator
#annotator = Annotator(senna_dir = "/Users/Guest/Documents/Yuxin/senna")
annotator = Annotator(senna_dir = "/Users/yuxin/senna")
# In[] 
from __future__ import unicode_literals, print_function
import spacy
from spacy.pipeline import DependencyParser
nlp = spacy.load('en')
import re
p = re.compile('#*[.,]?#+')
# In[]
def get_root(doc):
    root = None
    for token in doc:
        #print(token,token.dep_, token.head)
        if token.dep_ == "ROOT":
            root = token
            break
    return root
# In[]

def get_srl(line):
    sent = [line]
    srl = annotator.get_conll_format(sent, options=["-srl"])
    srl  = srl.split("\n")
    
    for i, item in enumerate(srl):
        item = item.split("\t")
        srl[i] = item
        for j, ele in enumerate(srl[i]):
            ele = ele.strip()
            srl[i][j] = ele
    return srl

def extract_verb(srl):
    verb_list = []
    for item in srl:
        if not item[1] == "-":
            verb_list.append(item[0])
    return verb_list

def extract_tag_col(srl,ncol):
    tag_col=[]
    for item in srl:
        if item[ncol] == "O":
            tag_col.append(item[ncol])
        else:
            tag_col.append(item[ncol].split("-"))
    return tag_col

def build_tag_idx(tag_col):
    tag_idx = []
    i = 0
    while i < len(tag_col):
        item = tag_col[i]
        if len(item)>1:
            if item[0]=="B":
                tag = "-".join(item[1::])
                j = i + 1
                while tag_col[j][0]!="E":
                    j+=1
                tag_idx.append(((i,j),tag))
                i = j + 1
            elif item[0]=="S":
                tag = "-".join(item[1::])
                tag_idx.append(((i,i),tag))
                i+=1
            else:
                i+=1
        else:
            i+=1
    return tag_idx

def augment(line): 
    doc = nlp(line.decode('utf-8'))
    #toks = [(str(token)) for token in doc]
    srl = get_srl(line)
    toks = [item[0] for item in srl]
    verb_list = extract_verb(srl)
    to_augment = False
    root=get_root(doc)
    if str(root) in verb_list:
        #print(verb_list.index(str(root)))
        tag_ncol = verb_list.index(str(root))+2
        to_augment = True
    elif len(verb_list)==1:
        tag_ncol = 2
        to_augment = True
    if to_augment:
        #print(tag_ncol)
        tag_col = extract_tag_col(srl,tag_ncol)
        tag_idx = build_tag_idx(tag_col)
        for item in tag_idx[::-1]:
            #print(item)
            (begin_idx,end_idx)=item[0]
            tag=item[1]
            toks.insert(end_idx+1,"E-"+tag)
            toks.insert(begin_idx,"B-"+tag)
    return toks
# In[]
    
#filepath = '/Users/yuxin/sumdata/train/train.title.txt'
filepath = "/Users/yuxin/sumdata/DUC2003/task1_ref0.txt"
#output_file = '/Users/yuxin/sumdata/train/train.title.augsrl.txt'
output_file = '/Users/yuxin/sumdata/train/DUC2003.title.augsrl.txt'
#filepath = "/Users/Guest/Documents/Yuxin/experiment/random.title.50.txt"
#output_file =  "/Users/Guest/Documents/Yuxin/experiment/random.title.50.augsrl.txt"
outF = open(output_file, "w")

with open(filepath) as fp:  
    line = fp.readline()
    count = 1
    while line:
        print(count)
        #print("{}".format(line.strip()))
        line = line.strip()
        #print(line)
        try:
            line = p.subn('#', line)[0]
            #print(line)
            augmented = augment(line)
            #print(" ".join(augmented))
            outF.write(" ".join(augmented))
        except:
            outF.write(line)
        outF.write("\n")                       
        line = fp.readline()
        count += 1
outF.close()