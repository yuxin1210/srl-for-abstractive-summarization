#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 13 21:32:13 2018

@author: yuxin
"""


import sys
folder_path = "/home/lr/yuxin/experiment/output/"
input_filename = folder_path + sys.argv[1]
input_filepath = input_filename + ".txt"
output_filename = input_filename + "_truncated"
output_filepath = output_filename + ".txt"
over75 = 0

outF = open(output_filepath, "w")
with open(input_filepath) as fp: 
    line = fp.readline()
    count = 1
    while line:
        #print(count)
        #print("{}".format(line.strip()))
        line = line.strip()
        length = len(line)
        if length >75:
            print(count,length)
            over75+=1
            outF.write(line[:75])
        else:
            outF.write(line)
        outF.write("\n")
        line = fp.readline()
        count += 1
outF.close()
print over75